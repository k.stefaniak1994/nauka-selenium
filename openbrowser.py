import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webelement import WebElement


@pytest.fixture
def base_url():
    return "http://192.168.0.69:8000/"


class TestNaukaSelenium:

    def test_open_browser(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        assert "Selenium practice - main page" == driver.title

    def test_locator_id(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url + "locators-id")
        lbl_header = driver.find_element(By.ID, "title-id")
        lbl_paragraph = driver.find_element(By.ID, "content")
        assert "Article title for ID locator" == lbl_header.text
        assert "Lorem ipsum dolor sit amet ID!" == lbl_paragraph.text

    def test_locator_name(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url + "locators-name")
        lbl_header_1 = driver.find_element(By.NAME, "title-name")
        lbl_paragraph_1 = driver.find_element(By.NAME, "content")
        assert "Article title for name locator" == lbl_header_1.text
        assert "Lorem ipsum dolor sit amet NAME!" == lbl_paragraph_1.text

    def test_locator_class(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url + "locators-class")
        lbl_header_2 = driver.find_element(By.NAME, "title-name")
        lbl_paragraph_2 = driver.find_element(By.CLASS_NAME, "dummy")
        lbl_cookies = driver.find_element(By.CLASS_NAME, "cookies")
        assert "Article title for class locators" == lbl_header_2.text
        assert "Here it is paragraph with dummy class. Paragraph below has other class." == lbl_paragraph_2.text
        assert "COOKIES!" == lbl_cookies.text

    def test_locator_tag(self, base_url, h1=None):
        driver = webdriver.Chrome()
        driver.get(base_url + "locators-tag-name")
        lbl_header_3 = driver.find_element(By.NAME, "title-name")
        lbl_tag_h2 = driver.find_element(By.TAG_NAME, "h2")

        assert "Article title for tag locators" == lbl_header_3.text
        assert "TAG H2" == lbl_tag_h2.text

    def test_locator_link(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url + "locators-link-text")
        lbl_header_4 = driver.find_element(By.ID, "title-id")
        lbl_content = driver.find_element(By.ID, "content")
        lbl_link_1 = driver.find_element(By.LINK_TEXT, "IMPORTANT LINK!")
        lbl_link_2 = driver.find_element(By.ID, "goBackButton")
        testowanko = driver.find_element(By.XPATH, "//a[@href='#']")

        assert "Article title for LINK locators" == lbl_header_4.text
        assert "Try to locate one of below links:" == lbl_content.text
        assert "IMPORTANT LINK!" == lbl_link_1.text
        assert "Back" == lbl_link_2.text
        assert "IMPORTANT LINK!" == testowanko.text
