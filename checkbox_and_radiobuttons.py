import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
import time


@pytest.fixture
def base_url():
    return "http://127.0.0.1:5000/#"

@pytest.fixture
def checkbox_url():
    return "http://127.0.0.1:5000/checkboxes-radiobuttons"


class TestCheckbox:

    def test_checkbox_click(self, checkbox_url):
        driver = webdriver.Chrome()

        driver.get(checkbox_url)
        apple_checkbox = driver.find_element(By.ID, "fruit1")

        apple_checkbox.click()

        time.sleep(3)
        assert apple_checkbox.is_selected()


    def test_checkbox_by_default(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

       checkbox_default = driver.find_element(By.ID, "fruit4")

        assert checkbox_default.is_selected()


    def test_checkbox_locked(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        checkbox_locked = driver.find_element(By.ID, "fruit5")

        assert not checkbox_locked.is_enabled()

class TestRadiobuttons:

    def test_radiobutton_up(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        direction_up_button = driver.find_element(By.ID, "up")
        direction_up_button.click()
      #  time.sleep(3)

        assert direction_up_button.is_selected()

    def test_radiobutton_left(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        direction_left_button = driver.find_element(By.ID, "left")

        time.sleep(2)

        direction_left_button.click()
        time.sleep(3)

        assert direction_left_button.is_selected()


    def test_radiobutton_right(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        direction_right_button = driver.find_element(By.ID, "right")
        direction_right_button.click()

        assert direction_right_button.is_selected()

    def test_radiobutton_down(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        direction_down_button = driver.find_element(By.ID, "down")
        direction_down_button.click()

        assert direction_down_button.is_selected()


    def test_satisfied_button(self, checkbox_url):
        driver = webdriver.Chrome()
        driver.get(checkbox_url)

        satisfied_button = driver.find_element(By.ID, "yes")

        assert satisfied_button.is_selected()


