import pyexpat
import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


@pytest.fixture
def base_url():
    return "http://127.0.0.1:5000/#"


class TestClassActions:

    def test_simple_actions(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)

        lbl_simple_actions_link = driver.find_element(By.XPATH, "//a[@href='/simple-actions']")
        lbl_simple_actions_link.click()

        egg_image = driver.find_element(By.XPATH, "//img[@src='static/egg.png']")
        actions = ActionChains(driver)
        actions.move_to_element(egg_image).double_click(egg_image).perform()

        crashed_egg_image = driver.find_element(By.XPATH, "//img[@src='static/crashed_egg.png']")
        expected_url = "http://127.0.0.1:5000/static/crashed_egg.png"

        assert crashed_egg_image.get_attribute("src") == expected_url

    def test_context_menu(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)

        lbl_context_menu_link = driver.find_element(By.XPATH, "//a[@href='/context-menu']")
        lbl_context_menu_link.click()

        context_menu = driver.find_element(By.ID, "contextMenuZone")

        actions = ActionChains(driver)
        actions.move_to_element(context_menu).context_click(context_menu).perform()

        super_button = driver.find_element(By.XPATH, "//button[contains(@onclick, 'confirmAction()')]")
        actions.click(super_button).perform()

        super_element = WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.ID, "message")))

        assert super_element.is_displayed()
