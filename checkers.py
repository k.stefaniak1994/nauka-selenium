import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time


@pytest.fixture
def base_url():
    return "http://127.0.0.1:5000/#"


@pytest.fixture
def check_url():
    return "http://127.0.0.1:5000/checkers"


class TestCheckers:

    def test_open_check_link(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)

        old_url = driver.current_url

        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/checkers']")
        lbl_login_link.click()

        WebDriverWait(driver, 300).until(EC.url_changes(old_url))

        new_url = driver.current_url

        assert old_url != new_url
        assert "http://127.0.0.1:5000/checkers" == new_url

    def test_check_element(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)

        old_url = driver.current_url

        lbl_checkers_link = driver.find_element(By.XPATH, "//a[@href='/checkers']")
        lbl_checkers_link.click()

        WebDriverWait(driver, 300).until(EC.url_changes(old_url))
       # new_url = driver.current_url

        checkbox = driver.find_element(By.ID, "checked_checkbox")
        checkbox.click()

        assert checkbox.is_selected()

    def test_check_element_is_not_hidden(self, check_url):
        driver = webdriver.Chrome()
        driver.get(check_url)

        button_hidden_element = driver.find_element(By.ID, "message_button")
        button_hidden_element.click()

        visible_text = driver.find_element(By.ID, "message")

        assert visible_text.is_displayed()
        assert "Text is now visible!" == visible_text.text

    def test_button_is_disabled(self, check_url):
        driver = webdriver.Chrome()
        driver.get(check_url)

        button_disabled = driver.find_element(By.ID, "disabled_button")

        assert not button_disabled.is_enabled()
