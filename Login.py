import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import time



@pytest.fixture
def base_url():
    return "http://192.168.0.69:8000/#"


class TestLoginLink:

    def test_find_login_link(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_main_page_login = driver.find_element(By.XPATH, "//a[@href='/login-form']")

        assert "Login form" == lbl_main_page_login.text

    def test_open_login_link(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        old_url = driver.current_url
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        WebDriverWait(driver, 10).until(EC.url_changes(old_url))
        new_url = driver.current_url

        assert old_url != new_url
        assert "http://192.168.0.69:8000/login-form" == new_url

    def test_log_in_to_account(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        old_url = driver.current_url
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        new_url = driver.current_url
        field_username = driver.find_element(By.ID, "username")
        field_username.send_keys("practitioner")
        field_password = driver.find_element(By.ID, "password")
        field_password.send_keys("c0rr3ctP4$$word")
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        WebDriverWait(driver, 10).until(EC.url_changes(old_url))
        error_message_1 = driver.find_element(By.ID, "status")
# do sprawdzenia w domu - net za wolny
        assert ">LOGIN SUCCESSFULLY!" == error_message_1

    def test_no_access_for_account(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        field_username = driver.find_element(By.ID, "username")
        field_username.send_keys("practitioner_no_permission")
        field_password = driver.find_element(By.ID, "password")
        field_password.send_keys("c0rr3ctP4$$word")
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        error_message_2 = driver.find_element(By.ID, "loginError")
        assert "No access for this account." == error_message_2.text

    def test_empty_field_error_message(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        empty_error_message = driver.find_element(By.ID, "loginError")

        assert "Please enter your login and password." == empty_error_message.text

    def test_empty_username_field_error_message(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        field_password = driver.find_element(By.ID, "password")
        field_password.send_keys("c0rr3ctP4$$word")
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        empty_uername_error_message = driver.find_element(By.ID, "loginError")

        assert "Please enter your login and password." == empty_uername_error_message.text

    def test_empty_password_field_message(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        field_username = driver.find_element(By.ID, "username")
        field_username.send_keys("practitioner_no_permission")
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        empty_password_error_message = driver.find_element(By.ID, "loginError")

        assert "Please enter your login and password." == empty_password_error_message.text

    def test_incorrect_login_data(self, base_url):
        driver = webdriver.Chrome()
        driver.get(base_url)
        lbl_login_link = driver.find_element(By.XPATH, "//a[@href='/login-form']")
        lbl_login_link.click()
        field_username = driver.find_element(By.ID, "username")
        field_username.send_keys("xyz")
        field_password = driver.find_element(By.ID, "password")
        field_password.send_keys("xyz")
        login_button = driver.find_element(By.ID, "button")
        login_button.click()
        incorrect_login_data = driver.find_element(By.ID, "loginError")

        assert "Incorrect login data." == incorrect_login_data.text


